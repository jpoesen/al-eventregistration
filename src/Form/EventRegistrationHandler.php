<?php
declare(strict_types=1);

namespace AdoptLondon\EventRegistration\Form;

use Wasp\Form\FormHandlerBase;
use WP_Post;

class EventRegistrationHandler extends FormHandlerBase {  

  public static function submit()
  {
   
    self::getData(); 

    if (!self::field_has_value('form_id', 'event-registration-form-submit')){
      return;
    }
    
    try {     

      if (!self::field_is_present("token")){
        throw new \Exception('Event registration error: security challenge failed.');
      }

      $event_id = self::$form_data['event'];
      $token = self::$form_data['token'];
      $action = "register-event_{$event_id}";

      if (wp_verify_nonce($token, $action) === false) {
        throw new \Exception('Event registration error: security challenge failed.');
      }

      // Load the event object.
      $event = get_post($event_id);

      if (!$event instanceof \WP_Post){
        throw new \Exception('Event registration error: event not an instance of WP_Post.');
      }

      $region = self::getRegion($event->ID);

      // Get region email.
      $region_email = get_post_meta($region->ID, 'contact_email', true);

      // Get event date.
      $event_date = get_post_meta($event->ID, 'date', true);
      $event_date = date('d/m/Y', strtotime($event_date));

      // Build mail subject.
      $subject = "Registration request for {$event->post_title}";

      // Build mail message.
      $message = self::build_message(
        self::$form_data['name'],
        self::$form_data['email'],
        self::$form_data['phone'],
        $event->post_title,
        $event_date,
        self::$form_data['referral'],
        'mail'
      );

      // Send mail.
      $mailto = WP_DEBUG ? getenv('DEBUG_EMAIL') : $region_email;
      $success = wp_mail($mailto, $subject, $message);

      if ($success === false) {
        throw new \Exception('Event registration error: problem sending mail. If WP_DEBUG is enabled, ensure DEBUG_EMAIL is set to a valid email address.');
      }

      // Log success.
      $log_message = self::build_message(
        self::$form_data['name'],
        self::$form_data['email'],
        self::$form_data['phone'],
        $event->post_title,
        $event_date,
        self::$form_data['referral'],
        'log'
      );
      \Wasp::service('logger')->log($log_message, 'eventreg-success');

      // Redirect to success page.
      wp_redirect('/event-registration-success');      
      
    }
    catch (\Throwable $e) {
      // Log error.
      //al_log($e->getMessage());
      \Wasp::service('logger')->log($e->getMessage(), 'eventreg-error');

      // Redirect to error page.
      wp_redirect('/event-registration-error');
    }

    exit();
  }

  private static function getRegion($event_id): \WP_Post {
      // Retrieve the region via the borough associated with the event.
      $borough_id = get_post_meta($event_id, 'borough', true);

      $regions = get_posts([
        'post_type' => 'region',
        'numberposts' => 1,
        'tax_query' => [
          [
            'taxonomy' => 'region_borough',
            'field' => 'term_id',
            'terms' => [$borough_id]
          ]
        ]
      ]);

      if (count($regions) <= 0){
        throw new \Exception('Event registration error: region missing.');
      }

      return $regions[0];
  }

  private static function build_message($name, $email, $phone, $event_title, $event_date, $referral, $message_type="mail"): string{

    $template = "";
    switch ($message_type) {
      case 'mail':
        $template = <<<'EOD'
        Name: %s
        Email: %s
        Phone No: %s
      
        Event: %s
        Event Date: %s
      
        How did you hear about us: %s
        EOD;      
        break;

      case 'log': {
        $template = "%s | %s | %s | %s | %s | %s";
      }
    }

    $message = sprintf(
      $template, 
      $name, 
      $email, 
      $phone, 
      $event_title,
      $event_date,
      $referral
    );

    return $message;
  }
}
