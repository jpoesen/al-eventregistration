<?php

declare(strict_types=1);

namespace AdoptLondon\EventRegistration;

use AdoptLondon\EventRegistration\Form\EventRegistrationHandler;
use Wasp\PluginBase;


class Plugin extends PluginBase {

  public static function get_actions() {
    return [
      'wp_loaded' => 'on_plugins_loaded'
    ];
  }

  public function on_plugins_loaded(){
    EventRegistrationHandler::submit();
  }

}