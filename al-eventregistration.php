<?php
/*
  Plugin Name: Adopt London Event Registration
  Plugin URI:
  Description: Handles event registrations
  Version: 1.0
  Author: Joeri Poesen
  Author URI: https://swiftcircle.com
  License: GPLv2
*/

namespace AdoptLondon\EventRegistration;

use Wasp\PluginManager;


$plugin = new Plugin();
$manager = new PluginManager();
$manager->register($plugin);